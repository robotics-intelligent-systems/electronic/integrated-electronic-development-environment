rem This is whre your Qt reside
rem %QT_HOME%\bin folder should contain all qt dlls and utilities, e.g qmake.exe, moc.exe ..
set QT_HOME=C:\usr\util\Qt\Qt5.7.0\5.7\mingw53_32

rem Please use only installation folders without any space in names.
rem Install latest Qt with MinGW for your platform (download: https://www.qt.io/download-open-source/#section-2)
rem Alter MINGW_HOME according to your installation:
set MINGW_HOME=C:\usr\util\Qt\Qt5.7.0\Tools\mingw530_32
rem Install Msys-1.0 into %MINGW_HOME%\msys\1.0 folder (look there: http://www.mingw.org/wiki/MSYS and there: https://wiki.qt.io/MinGW-64-bit)

rem Install latest cmake utility (https://cmake.org/download/) and update CMAKE_HOME variable accordingly:
set CMAKE_HOME=C:\usr\util\cmake-3.7.1-win32-x86
::C:\usr\util\cmake-3.6.1-win32-x86

rem Install WinAVR (look there: https://sourceforge.net/projects/winavr/)
rem WinAVR installer should automatically put the AVR32_HOME variable into the system environment:
rem set AVR32_HOME=C:\usr\util\WinAVR-20100110\avr

rem Now open a command shell prepared by your Qt installation,
rem see the Qt-related System Start Menu shortcuts, something like e.g. "Qt 5.7 for Desktop (MinGW 5.3.0 32 bit)",
rem	cd to the sumutron sources root folder (where build-mingw.bat reside)
rem and run this script.

setlocal

set ORIG_PATH=%PATH%
rem set PATH=%MINGW_HOME%\msys\1.0\bin;%MINGW_HOME%\bin;%ORIG_PATH%;%CMAKE_HOME%\bin
set PATH=%QT_HOME%\bin;%MINGW_HOME%\bin;%MINGW_HOME%\msys\1.0\bin;%ORIG_PATH%;%CMAKE_HOME%\bin

for /f "usebackq tokens=*" %%a in (`cd`) do set CWD=%%a
set TARGET_DIR=%CWD:\=/%
set CC=gcc
set CFLAGS=-I%TARGET_DIR%/include
set LFLAGS=-L%TARGET_DIR%/lib

rem goto simavr
::goto build_plugins

@echo *******************************************************
@echo Building dlfcn-win32
@echo *******************************************************
cd dependencies\dlfcn-win32
mkdir build
cd build
cmake -G "MSYS Makefiles" -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=%CWD% ..
rem cmake --build . --target install
make clean
make DESTDIR="" install
rd /S /Q build

rem goto fine

:libelf
@echo ' '
@echo ' '
@echo *******************************************************
@echo Building libelf-0.8.13
@echo *******************************************************
rem set CFLAGS=-I%MINGW_HOME%\msys\1.0\include
cd %CWD%
cd dependencies\libelf-0.8.13
del config.cache /f /q
bash -c "./configure --prefix=%TARGET_DIR%"
make clean
env  make install

:simavr
@echo ' '
@echo ' '
@echo *******************************************************
@echo Building simavr
@echo *******************************************************
cd %CWD%
cd dependencies\simavr
make clean RELEASE=1
make build-simavr CC=gcc WIN=Msys CFLAGS=-I%TARGET_DIR%/include LFLAGS=-L%TARGET_DIR%/lib RELEASE=1
make install CC=gcc WIN=Msys CFLAGS=-I%TARGET_DIR%/include LFLAGS=-L%TARGET_DIR%/lib DESTDIR=%TARGET_DIR% RELEASE=1

rem goto fine

:simutron
cd %CWD%
@echo ' '
@echo ' '
@echo *******************************************************
@echo Building simutron main executable file
@echo *******************************************************
mkdir build
qmake simutronqt4.pro
rem make clean
make

:build_plugins
@echo ''
@echo ''
@echo *******************************************************
@echo Building plugins
@echo *******************************************************

cd %CWD%\plugins

rd /S /Q debug
rd /S /Q release
rd /S /Q build

for /d %%G in (*) do call :build_plugin %%G

goto fine


:build_plugin
@echo *********
@echo Building plugin: %*
@echo *********
cd %CWD%\plugins\%*
for /d %%G in (comp_li?) do call :build_fld_comp_lib %*
for /d %%G in (plugi?) do call :build_fld_plugin %*
cd %CWD%\plugins
exit /b 0

:build_fld_comp_lib
@echo ******
@echo Building in comp_lib folder of %*
@echo ******
cd comp_lib
@echo ****** running qmake
qmake *.pro
::-r -spec win32-g++ "CONFIG+=release"
@echo ****** qmake result: %ERRORLEVEL%
make clean
make
rd /S /Q debug
rd /S /Q release
rd /S /Q build
cd %CWD%\plugins\%*
exit /b 0

:build_fld_plugin
@echo ******
@echo Building in plugin folder of %*
@echo ******
cd plugin
@echo ****** running qmake
qmake *.pro
::-r -spec win32-g++ "CONFIG+=release"
@echo ****** qmake result: %ERRORLEVEL%
make clean
make
rd /S /Q debug
rd /S /Q release
rd /S /Q build
cd %CWD%\plugins\%*
exit /b 0

:fine
cd %CWD%
rd /S /Q debug
rd /S /Q release
rd /S /Q build

endlocal
exit /b %ERRORLEVEL%
