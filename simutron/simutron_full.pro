TEMPLATE=subdirs

SUBDIRS=../src/simutron_app.pro \
    ../plugins/rencoder/plugin/rencoder.pro \
    ../plugins/pushbutton/plugin/pushbutton.pro \
    ../plugins/pcd8544/plugin/pcd8544.pro \
    ../plugins/hd44780/plugin/hd44780.pro \
    ../plugins/lfd4155/plugin/lfd4155.pro \
    ../plugins/counter/plugin/bincounter.pro \
    ../plugins/dds/plugin/dds.pro \
    ../plugins/fulladder/plugin/fulladder.pro

#    ../plugins/avrmcu/plugin/avrmcu.pro \
#    ../plugins/avrserialterm/plugin/avrserialterm.pro
