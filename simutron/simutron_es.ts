<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>AndGate</name>
    <message>
        <location filename="src/items/components/gate_and.cpp" line="35"/>
        <source>And Gate</source>
        <translation type="unfinished">Puerta And</translation>
    </message>
    <message>
        <location filename="src/items/components/gate_and.cpp" line="36"/>
        <source>Logic</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AndGatethree</name>
    <message>
        <location filename="src/items/components/gate_and3.cpp" line="35"/>
        <source>And G. 3 inp.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/items/components/gate_and3.cpp" line="36"/>
        <source>And Gate</source>
        <translation type="unfinished">Puerta And</translation>
    </message>
</context>
<context>
    <name>Buffer</name>
    <message>
        <location filename="src/items/components/buffer.cpp" line="36"/>
        <source>Buffer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/items/components/buffer.cpp" line="37"/>
        <source>Logic</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CircuitWidget</name>
    <message>
        <location filename="src/gui/circuitwidget.cpp" line="45"/>
        <source>document1.circuit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorCombo</name>
    <message>
        <location filename="src/gui/QPropertyEditor/ColorCombo.cpp" line="36"/>
        <source>Custom</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FlipFlopJK</name>
    <message>
        <location filename="src/items/components/FlipFlopJK.cpp" line="35"/>
        <source>J-K FlipFlop</source>
        <translation type="unfinished">Biestable J-K</translation>
    </message>
    <message>
        <location filename="src/items/components/FlipFlopJK.cpp" line="36"/>
        <source>FlipFlops</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Ground</name>
    <message>
        <location filename="src/items/components/ground.cpp" line="37"/>
        <source>Ground (0 V)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/items/components/ground.cpp" line="38"/>
        <source>Sources</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Inverter</name>
    <message>
        <location filename="src/items/components/inverter.cpp" line="36"/>
        <source>Inverter</source>
        <translation type="unfinished">Inversor</translation>
    </message>
    <message>
        <location filename="src/items/components/inverter.cpp" line="37"/>
        <source>Logic</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LogicInput</name>
    <message>
        <location filename="src/items/components/logicinput.cpp" line="36"/>
        <source>Fixed Voltage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/items/components/logicinput.cpp" line="37"/>
        <source>Sources</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LogicOutput</name>
    <message>
        <location filename="src/items/components/logicoutput.cpp" line="36"/>
        <source>Logic Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/items/components/logicoutput.cpp" line="37"/>
        <source>Outputs</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PICComponent</name>
    <message>
        <location filename="src/items/pic/piccomponent.cpp" line="36"/>
        <location filename="src/items/pic/piccomponent.cpp" line="39"/>
        <source>PIC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/items/pic/piccomponent.cpp" line="37"/>
        <source>Integrated Circuits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/items/pic/piccomponent.cpp" line="50"/>
        <source>&lt;Enter location of PIC Program&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Pin</name>
    <message>
        <location filename="src/items/components/pin.cpp" line="36"/>
        <source>Pin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/items/components/pin.cpp" line="37"/>
        <source>Outputs</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QPropertyModel</name>
    <message id="Qt::DisplayRole">
        <location filename="src/gui/QPropertyEditor/QPropertyModel.cpp" line="151"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/QPropertyEditor/QPropertyModel.cpp" line="153"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Resistor</name>
    <message>
        <location filename="src/items/components/resistor.cpp" line="37"/>
        <source>Resistor</source>
        <translation type="unfinished">Resistencia</translation>
    </message>
    <message>
        <location filename="src/items/components/resistor.cpp" line="38"/>
        <source>Passive</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>simutronQt4</name>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="99"/>
        <source>About Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="100"/>
        <source>This is a circuit simulator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="130"/>
        <location filename="src/core/simutronqt4.cpp" line="149"/>
        <location filename="src/core/simutronqt4.cpp" line="157"/>
        <location filename="src/core/simutronqt4.cpp" line="177"/>
        <source>Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="131"/>
        <source>The document has been modified.
Do you want to save your changes?</source>
        <translation type="unfinished">El circuito ha sido modificado.
Quiere guardar los cambios?</translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="150"/>
        <source>Cannot read file %1:
%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="158"/>
        <source>Cannot set file %1
to DomDocument</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="168"/>
        <source>File loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="178"/>
        <source>Cannot write file %1:
%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="227"/>
        <source>File saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="243"/>
        <source>%1[*] - %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="243"/>
        <source>SimutronQt4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="296"/>
        <location filename="src/core/simutronqt4.cpp" line="432"/>
        <location filename="src/core/simutronqt4.cpp" line="448"/>
        <location filename="src/core/simutronqt4.cpp" line="457"/>
        <source>Continuous</source>
        <translation type="unfinished">Continuo</translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="297"/>
        <location filename="src/core/simutronqt4.cpp" line="438"/>
        <source>Step</source>
        <translation type="unfinished">Paso a Paso</translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="318"/>
        <source>&amp;New</source>
        <translation type="unfinished">Nuevo</translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="319"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="320"/>
        <source>Create a new file</source>
        <translation type="unfinished">Crear nuevo circuito</translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="322"/>
        <source>&amp;Open...</source>
        <translation type="unfinished">Abrir</translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="323"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="324"/>
        <source>Open an existing file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="327"/>
        <source>&amp;Save</source>
        <translation type="unfinished">Guardar</translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="328"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="329"/>
        <source>Save the document to disk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="332"/>
        <source>Save &amp;As...</source>
        <translation type="unfinished">Guardar como</translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="333"/>
        <source>Save the document under a new name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="336"/>
        <source>E&amp;xit</source>
        <translation type="unfinished">Salir</translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="337"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="338"/>
        <source>Exit the application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="341"/>
        <source>Cu&amp;t</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="342"/>
        <source>Ctrl+X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="343"/>
        <source>Cut the current selection&apos;s contents to the clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="347"/>
        <source>&amp;Copy</source>
        <translation type="unfinished">Copiar</translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="348"/>
        <source>Ctrl+C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="349"/>
        <source>Copy the current selection&apos;s contents to the clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="353"/>
        <source>&amp;Paste</source>
        <translation type="unfinished">Pegar</translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="354"/>
        <source>Ctrl+V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="355"/>
        <source>Paste the clipboard&apos;s contents into the current selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="359"/>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="360"/>
        <source>Show the application&apos;s About box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="363"/>
        <source>About &amp;Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="364"/>
        <source>Show the Qt library&apos;s About box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="374"/>
        <source>Start/Step</source>
        <translation type="unfinished">Iniciar/Parar</translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="375"/>
        <source>Run simulation or run Step</source>
        <translation type="unfinished">Continuo o Paso a Paso</translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="379"/>
        <source>Pause</source>
        <translation type="unfinished">Pausa</translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="380"/>
        <source>Pause simulation</source>
        <translation type="unfinished">Pausar simulación</translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="388"/>
        <source>&amp;File</source>
        <translation type="unfinished">Archivo</translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="396"/>
        <source>&amp;Edit</source>
        <translation type="unfinished">Editar</translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="403"/>
        <source>&amp;Help</source>
        <translation type="unfinished">Ayuda</translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="410"/>
        <source>File</source>
        <translation type="unfinished">Archivo</translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="416"/>
        <source>Edit</source>
        <translation type="unfinished">Editar</translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="422"/>
        <source>Tools</source>
        <translation type="unfinished">Herramientas</translation>
    </message>
    <message>
        <location filename="src/core/simutronqt4.cpp" line="483"/>
        <source>Ready</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
