#!/bin/bash
#

# This script on Linux builds simutron for Windows.
# Relies on the MXE cross-platform build environment, see http://mxe.cc

_ARCH=Win
_BITS=32

if [[ "x${LINKAGE}" == "x" ]]; then
	LINKAGE=shared
fi

if [ $_BITS -eq 32 ]; then
  TRGT_PROC=i686
else #[ $_BITS -eq 64 ]; then
  TRGT_PROC=x86_64
  _BITS=64
fi


unset INCLUDE
unset LIB
export LANG=en_US.UTF-8

TOPDIR=`pwd`
TARGET_PREFIX=$TOPDIR
BUILD_DIR=$TOPDIR

export QT_SELECT=5


TRGT_KEY=$TRGT_PROC-w64-mingw32.$LINKAGE
TOOLCHAIN_DIR=$(dirname "`which $TRGT_KEY-gcc`")
TOOLCHAIN_DIR=$(dirname "$TOOLCHAIN_DIR")
echo "MXEDIR=$TOOLCHAIN_DIR"


findQtTool() {
  local _tool_path=`find "$TOOLCHAIN_DIR" -name $1 |grep "\.$LINKAGE\/" |grep "$TRGT_PROC" |grep "\/qt$QT_SELECT\/bin\/"`
  echo $_tool_path
}

#QMAKE=`findQtTool qmake-qt$QT_SELECT`

which $TRGT_KEY-qmake-qt5 2>&1 >> /dev/null
qmake_avlbl=$?

if [ $qmake_avlbl -eq 0 ]; then
  QMAKE=$TRGT_KEY-qmake-qt5
else
  zenity --warning --text="Please install Qt5 development environment"
  exit 1
fi

QTDIR="$TOOLCHAIN_DIR"/$TRGT_KEY/qt$QT_SELECT
#export PATH="$QTDIR"/bin:$PATH

echo "QTDIR=$QTDIR"

$QMAKE --version

export MOC=`findQtTool moc`
export UIC=`findQtTool uic`
export QDBUSCPP2XML=`findQtTool qdbuscpp2xml`
export QDBUSXML2CPP=`findQtTool qdbusxml2cpp`
export QDOC=`findQtTool qdoc`
export RCC=`findQtTool rcc`
export SYNCQT_PL=`findQtTool syncqt.pl`
#export LINGUIST=/usr/$TRGT_KEY/sys-root/mingw/bin/linguist-qt5.exe

export CC="$TRGT_KEY-gcc"
export CXX="$TRGT_KEY-g++"
export AR="$TRGT_KEY-gcc-ar"
export RANLIB="$TRGT_KEY-gcc-ranlib"
export NM="$TRGT_KEY-gcc-nm"
export GCOV="$TRGT_KEY-gcov"
export CMAKE="$TRGT_KEY-cmake"

which ${CMAKE} 2>&1 >> /dev/null
mxe_cmake_avlbl=$?
if [ $mxe_cmake_avlbl -ne 0 ]; then
  export CMAKE=
fi

export "PKG_CONFIG_PATH_"$TRGT_PROC"_w64_mingw32_"$LINKAGE="$TOPDIR"/lib/pkgconfig
export "PKG_CONFIG_PATH_"$TRGT_PROC"_w64_mingw32_"$LINKAGE="$TOPDIR"/lib/pkgconfig

export AVR32_HOME=`which avr-gcc |sed 's/^\(.*\)\/bin\/avr-gcc$/\1/'`
if [ -d $AVR32_HOME/avr ]; then
  echo "avr-gcc toolchain found"
  AVR_INC=$AVR32_HOME/avr
else
  #Strange. My be ccache installed?
  #Fall back to default system location
  AVR32_HOME=/usr
  if [ -d $AVR32_HOME/lib/avr ]; then
    AVR_INC=$AVR32_HOME/lib/avr
  elif [ -d $AVR32_HOME/avr ]; then
    AVR_INC=$AVR32_HOME/avr
  else
    zenity --warning --text="Please install linux avr-gcc toolchain"
    exit 1
  fi
fi
echo "$AVR32_HOME will be used as avr-gcc home"
echo " "
echo " "

#rm -r lib
rm -r include
rm -r debug
rm -r release
rm bin/*.dll
mkdir -p "$TARGET_PREFIX"/bin
mkdir -p "$TARGET_PREFIX"/lib

cp "$TOOLCHAIN_DIR"/$TRGT_KEY/bin/libbz2.dll "$TOPDIR"/bin
#cp "$TOOLCHAIN_DIR"/$TRGT_KEY/bin/libdl.dll "$TOPDIR"/bin
cp "$TOOLCHAIN_DIR"/$TRGT_KEY/bin/libfreetype-6.dll "$TOPDIR"/bin
cp "$TOOLCHAIN_DIR"/$TRGT_KEY/bin/libgcc_s_seh-1.dll "$TOPDIR"/bin
cp "$TOOLCHAIN_DIR"/$TRGT_KEY/bin/libgcc_s_sjlj-1.dll "$TOPDIR"/bin
cp "$TOOLCHAIN_DIR"/$TRGT_KEY/bin/libglib-2.0-0.dll "$TOPDIR"/bin
cp "$TOOLCHAIN_DIR"/$TRGT_KEY/bin/libharfbuzz-0.dll "$TOPDIR"/bin
cp "$TOOLCHAIN_DIR"/$TRGT_KEY/bin/libiconv-2.dll "$TOPDIR"/bin
cp "$TOOLCHAIN_DIR"/$TRGT_KEY/bin/libintl-8.dll "$TOPDIR"/bin
cp "$TOOLCHAIN_DIR"/$TRGT_KEY/bin/libjpeg-9.dll "$TOPDIR"/bin
cp "$TOOLCHAIN_DIR"/$TRGT_KEY/bin/libpcre-1.dll "$TOPDIR"/bin
cp "$TOOLCHAIN_DIR"/$TRGT_KEY/bin/libpcre2-16-0.dll "$TOPDIR"/bin
cp "$TOOLCHAIN_DIR"/$TRGT_KEY/bin/libpng16-16.dll "$TOPDIR"/bin
cp "$TOOLCHAIN_DIR"/$TRGT_KEY/bin/libstdc++-6.dll "$TOPDIR"/bin
cp "$TOOLCHAIN_DIR"/$TRGT_KEY/bin/libwinpthread-1.dll "$TOPDIR"/bin
cp "$TOOLCHAIN_DIR"/$TRGT_KEY/bin/zlib1.dll "$TOPDIR"/bin

cp "$QTDIR"/bin/Qt5Core.dll "$TOPDIR"/bin
cp "$QTDIR"/bin/Qt5Gui.dll "$TOPDIR"/bin
cp "$QTDIR"/bin/Qt5Svg.dll "$TOPDIR"/bin
cp "$QTDIR"/bin/Qt5Widgets.dll "$TOPDIR"/bin
cp "$QTDIR"/bin/Qt5Xml.dll "$TOPDIR"/bin
cp "$QTDIR"/bin/Qt5SerialPort.dll "$TOPDIR"/bin

rm -r "$TOPDIR"/bin/plugins
rm -r "$TOPDIR"/bin/platforms
cp -r $QTDIR/plugins "$TOPDIR"/bin
cp -r $QTDIR/plugins/platforms "$TOPDIR"/bin

echo " "
echo " "
echo "*******************************************************"
echo "Building libglibc_win (provides strsep function for simavr)"
echo "*******************************************************"
cd $TOPDIR/dependencies/libglibc_win
rm -r build_${TRGT_KEY}
mkdir build_${TRGT_KEY}
rm -f config.status
rm -f aclocal.m4
rm -r autom4te.cache
rm -r .deps
aclocal
autoconf
automake --add-missing
chmod u+rx ./configure
cd build_${TRGT_KEY}
../configure --host=${TRGT_KEY} --prefix="$TARGET_PREFIX"
#--disable-shared 
make clean
make install
ret=$?
if [ $ret -ne 0 ]; then
  cd $BUILD_DIR
  echo libglibc_win build failed
  exit 1
fi
#export PKG_CONFIG_PATH=$TOPDIR/lib/pkgconfig:$PKG_CONFIG_PATH
#export LD_LIBRARY_PATH=$TOPDIR/lib:$LD_LIBRARY_PATH

#cd $BUILD_DIR
#exit 0


echo " "
echo " "
echo "*******************************************************"
echo Building dlfcn-win32
echo "*******************************************************"
cd $TOPDIR/dependencies/dlfcn-win32
rm -r build_${TRGT_KEY}
mkdir build_${TRGT_KEY}
cd build_${TRGT_KEY}
if [[ "x${CMAKE}" == "x" ]]; then
cat >mingw_cross_toolchain.cmake <<'EOL'
SET(CMAKE_SYSTEM_NAME Windows)
include(CMakeForceCompiler)
# Prefix detection only works with compiler id "GNU"
CMAKE_FORCE_C_COMPILER(${GNU_HOST}-gcc GNU)
# CMake doesn't automatically look for prefixed 'windres', do it manually:
SET(CMAKE_RC_COMPILER ${GNU_HOST}-windres)
EOL
cmake \
-D CMAKE_BUILD_TYPE=Release \
-D GNU_HOST=$TRGT_KEY \
-D CMAKE_TOOLCHAIN_FILE=mingw_cross_toolchain.cmake \
-D CMAKE_INSTALL_PREFIX="$TARGET_PREFIX" \
..
ret=$?
else
${CMAKE} -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="$TARGET_PREFIX" ..
ret=$?
fi
if [ $ret -ne 0 ]; then
  cd $BUILD_DIR
  echo dlfcn-win32 build failed
  exit 1
fi
make clean
make DESTDIR="" install

#cd $BUILD_DIR
#exit 0

echo ""
echo ""
echo "*******************************************************"
echo Building libelf-0.8.13
echo "*******************************************************"
cd $TOPDIR/dependencies/libelf-0.8.13
rm -f config.cache
rm -f confdefs.h
rm -f config.h
rm -f lib/sys_elf.h
chmod u+rx ./configure
chmod u+rx ./config.guess
chmod u+rx ./config.sub
rm -r build_${TRGT_KEY}
mkdir build_${TRGT_KEY}
cd build_${TRGT_KEY}
libelf_cv_int64='long long' libelf_cv_int32=int libelf_cv_int16=short ../configure \
    --host=$TRGT_KEY --enable-elf64 --enable-compat --prefix="$TARGET_PREFIX"
#--disable-shared 
make clean
make install
ret=$?
if [ $ret -ne 0 ]; then
  cd $BUILD_DIR
  echo libelf-0.8.13 build failed
  exit 1
fi
#export PKG_CONFIG_PATH=$TOPDIR/lib/pkgconfig:$PKG_CONFIG_PATH
#export LD_LIBRARY_PATH=$TOPDIR/lib:$LD_LIBRARY_PATH

#cd $BUILD_DIR
#exit 0

##xif ${MAIN_STREAM_SIMAVR}
#fi

echo ""
echo ""
echo "******************************************************"
echo Building libsimavr
echo "******************************************************"
cd $TOPDIR
mkdir include
cd $TOPDIR/include
cat >string.h <<'EOL'
#ifndef GNUSTRING_WIN_H
#define GNUSTRING_WIN_H
#if defined __MINGW32__
extern char *strsep (char **__restrict __stringp,
			 const char *__restrict __delim)
	// __THROW __nonnull ((1, 2))
	;
#endif //__MINGW32__
#include_next <string.h>
#endif //GNUSTRING_WIN_H
EOL
#cd $TOPDIR/dependencies/simavr
#export LDFLAGS=
#export LFLAGS=
#LFLAGS_SIMAVR="-L$TOPDIR/lib -L$TOPDIR/dependencies/simavr/simavr/obj-$TRGT_KEY -lsimavr -lm -lelf -lws2_32 -lcwin"
#make clean RELEASE=1
#make build-simavr WIN=Msys CFLAGS=-I$TOPDIR/include RELEASE=1 LFLAGS="" LDFLAGS="$LFLAGS_SIMAVR"
#make install WIN=Msys CFLAGS=-I$TOPDIR/include DESTDIR=$TOPDIR RELEASE=1 LFLAGS="" LDFLAGS="$LFLAGS_SIMAVR"

cd $TOPDIR/dependencies/simavr/examples
cat >Makefile <<'EOL'
boards=board_ds1338 board_i2ctest
all:
	for bi in ${boards}; do echo $$bi; $(MAKE) -C $$bi; done

clean:
	for bi in ${boards}; do echo $$bi; $(MAKE) -C $$bi clean; done
EOL
cd $TOPDIR/dependencies/simavr/examples/parts
cat >Makefile <<'EOL'
LDFLAGS += -lpthread
simavr = ../../
IPATH += ${simavr}/simavr/sim

target := libsimavrparts

all: obj ${target}

include ${simavr}/Makefile.common

objects := $(patsubst %.c,${OBJ}/%.o, $(wildcard *.c))

# Unless the VHCI_USB env var is set, don't build this because it requires
# a supporting library to be installed. See examples/extra_board_usb/README
# for details on how to build it.
ifndef VHCI_USB
	objects := $(filter-out %vhci_usb.o, $(objects))
endif
# Unless the USE_GLUT env var is set, don't build this because it requires
# a supporting library to be installed.
ifndef USE_GLUT
	objects := $(filter-out %glut.o, $(objects))
endif
objects := $(filter-out %pty.o, $(objects))

#
# Static library
#
${OBJ}/${target}.a: ${objects}
${target}: ${OBJ}/${target}.a
#
# Shared library (Linux only)
#
ifeq (${shell uname}, Linux)
${target}: ${OBJ}/${target}.so
endif

clean: clean-${OBJ}
	rm -rf *.hex *.a *.axf *.vcd .*.swo .*.swp .*.swm .*.swn *.so *.o
EOL
cd $TOPDIR/dependencies/simavr
export LDFLAGS=
export LFLAGS=
LFLAGS_SIMAVR="-L${TARGET_PREFIX}/lib -L${TOPDIR}/dependencies/simavr/simavr/obj-${TRGT_KEY} -lsimavr -lm -lelf -lws2_32 -lglibc_win"
cat >uname <<EOF
#!/bin/bash
echo Msys
EOF
chmod ugo+rx ./uname
env PATH=$TOPDIR/dependencies/simavr:$PATH make clean RELEASE=1
env PATH=$TOPDIR/dependencies/simavr:$PATH make all WIN=Msys AVR_INC=$AVR_INC CFLAGS="-I${TARGET_PREFIX}/include -I${TOOLCHAIN_DIR}/${TRGT_KEY}/include -O3 -Wall" \
RELEASE=1 LFLAGS="" LDFLAGS="$LFLAGS_SIMAVR" EXTRA_LDFLAGS="$LFLAGS_SIMAVR"
env PATH=$TOPDIR/dependencies/simavr:$PATH make install WIN=Msys CFLAGS="-I${TARGET_PREFIX}/include -I${TOOLCHAIN_DIR}/${TRGT_KEY}/include -O3 -Wall" \
DESTDIR=${TARGET_PREFIX} RELEASE=1 LFLAGS="" LDFLAGS="$LFLAGS_SIMAVR" EXTRA_LDFLAGS="$LFLAGS_SIMAVR"
mv -f ${TARGET_PREFIX}/bin/simavr ${TARGET_PREFIX}/bin/simavr.exe




#echo "LFLAGS_SIMAVR='$LFLAGS_SIMAVR'"


#cd $TOPDIR
#exit 0

#export PKG_CONFIG_PATH=$TOPDIR/lib/pkgconfig:$PKG_CONFIG_PATH
#export LD_LIBRARY_PATH=$TOPDIR/lib:$LD_LIBRARY_PATH

echo " "
echo " "
echo "******************************************************"
echo Building simutron main executable & plugins
echo "******************************************************"
cd $TOPDIR/build
$QMAKE -config release *.pro
ret=$?
if [ $ret -eq 0 ]; then
  make clean
  make
  ret=$?
fi
if [ $ret -ne 0 ]; then
  echo simutron build failed
  exit 1
fi

if [ -d $TOPDIR/dependencies/SerialConsole ]; then
echo ""
echo ""
echo "*******************************************************"
echo Building SerialConsole
echo "*******************************************************"
cd $TOPDIR/dependencies/SerialConsole
make clean -f Makefile.$TRGT_PROC
make -f Makefile.$TRGT_PROC
ret=$?
if [ $ret -ne 0 ]; then
  cd $BUILD_DIR
  echo SerialConsole build failed
  exit 1
fi

fi

#cd $BUILD_DIR
#exit 0

cd $TOPDIR

echo " "
echo " "
echo "*******************************************************"
echo Build done
echo "*******************************************************"

exit 0

echo ""
echo ""
echo "******************************************************"
echo Building plugins
echo "******************************************************"

for plugin_dir in $TOPDIR/plugins/*/
do
  plugin_name=`echo $plugin_dir |sed 's/.*\/\([^\/]\+\)\/$/\1/'`
  echo "Building plugin $plugin_name"
  if [ -d $TOPDIR/plugins/$plugin_name/comp_lib ]; then
    cd $TOPDIR/plugins/$plugin_name/comp_lib
    $QMAKE *.pro
    ret=$?
    if [ $ret -eq 0 ]; then
      make clean
      make
      ret=$?
    fi
    if [ $ret -ne 0 ]; then
      echo "Build failed on $plugin_name"
      #exit 1
    fi
    rm -r build
  fi
  if [ -d $TOPDIR/plugins/$plugin_name/plugin ]; then
    cd $TOPDIR/plugins/$plugin_name/plugin
    $QMAKE *.pro
    ret=$?
    if [ $ret -eq 0 ]; then
      make clean
      make
      ret=$?
    fi
    if [ $ret -ne 0 ]; then
      echo "Build failed on $plugin_name"
      #exit 1
    fi
    rm -r build
  fi
done

cd $TOPDIR

echo " "
echo " "
echo "*******************************************************"
echo Build done
echo "*******************************************************"
